<?php

require_once "../src/ConnectDataBase.php";
require_once "../src/DataBaseSensor.php";
require_once "../src/DataBaseReport.php";
require_once "../src/WorkCalculator.php";
if (!isset($_GET['pageId'])) {
    require_once "../template/index.php";
}
if (isset($_GET['pageId']) && !empty($_GET['pageId'])) {
    $pageId = filter_input(INPUT_GET, "pageId", FILTER_SANITIZE_STRING);
    if ($pageId == "index") {
        if (isset($_POST['sensorData']) && !empty($_POST['sensorData'])) {
            $sensorData = filter_input(INPUT_POST, "sensorData", FILTER_SANITIZE_NUMBER_INT);
            $dataBaseSensor = new DataBaseSensor();
            $dataBaseSensor->delete($sensorData);
        }

        require_once "../template/index.php";
    } elseif ($pageId == "create") {
        if (isset($_POST['sensor_name']) && isset($_POST['sensor_value']) && isset($_POST['sensor_unit'])) {
            if (!empty($_POST['sensor_name']) && !empty($_POST['sensor_value']) && !empty($_POST['sensor_unit'])) {
                $dataBaseSensor = new DataBaseSensor();
                $sensorName = filter_input(INPUT_POST, "sensor_name", FILTER_SANITIZE_STRING);
                $sensorFormat = filter_input(INPUT_POST, "sensor_unit", FILTER_SANITIZE_STRING);
                $sensorValue = filter_input(INPUT_POST, "sensor_value", FILTER_SANITIZE_STRING);
                $dataBaseSensor->create($sensorName, $sensorFormat, $sensorValue);
                header("location: " . "/index.php?action=index");
            } else {
                $validationErrorText = " Заполните все поля";
            }
        }
        require_once "../template/create.php";
    } elseif ($pageId == "report") {
        if (isset($_POST['sensorFormat']) && !empty($_POST['sensorFormat'])) {
            $dataBaseReport = new DataBaseReport();
            $workCalculator = new WorkCalculator();
            $sensorFormat = filter_input(INPUT_POST, "sensorFormat", FILTER_SANITIZE_NUMBER_INT);
            $sensorFormat = $sensorFormat == WorkCalculator::FORMAT_CELSIUS ? WorkCalculator::FORMAT_CELSIUS : WorkCalculator::FORMAT_FARENHEIT;
            $sensorValue = $workCalculator->convertingSensors($sensorFormat);
            $dataBaseReport->report($sensorFormat, $sensorValue);
        }
        require_once "../template/report.php";
    } elseif ($pageId == "history") {
        if (isset($_POST['sensorReport']) && !empty($_POST['sensorReport'])) {
            $sensorReport = filter_input(INPUT_POST, "sensorReport", FILTER_SANITIZE_NUMBER_INT);
            $dataBaseReport = new DataBaseReport();
            $dataBaseReport->delete($sensorReport);
        }
        require_once "../template/history.php";
    } elseif ($pageId == "edit" && isset($_GET['id']) && !empty($_GET['id'])) {
        if (isset($_POST['sensor_name']) && isset($_POST['sensor_value']) && isset($_POST['sensor_unit'])) {
            $dataBaseSensor = new DataBaseSensor();
            $sensorName = filter_input(INPUT_POST, "sensor_name", FILTER_SANITIZE_STRING);
            $sensorFormat = filter_input(INPUT_POST, "sensor_unit", FILTER_SANITIZE_STRING);
            $sensorValue = filter_input(INPUT_POST, "sensor_value", FILTER_SANITIZE_STRING);
            $sensorId = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
            $dataBaseSensor->edit($sensorName, $sensorFormat, $sensorValue, $sensorId);
            header("location:/index.php?pageId=index");
        }

        require_once "../template/edit.php";
    } else {
        echo "Not found";
    }
}
