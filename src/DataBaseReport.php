<?php

require_once "../src/ConnectDataBase.php";

class DataBaseReport extends ConnectDataBase
{
    /**
     * @return mixed
     */
    public function getReport()
    {
        $prepare = $this->connect->prepare('SELECT * FROM report');
        $prepare->execute();
        return $prepare->fetchALL(PDO::FETCH_OBJ);
    }

    /**
     * @param integer $sensorFormat
     * @param float|void $sensorValue
     * @return void
     */
    public function report($sensorFormat, $sensorValue)
    {
        $prepare = $this->connect->prepare('INSERT INTO report(date, unit, value) VALUES (:date, :unit, :value)');
        $prepare->bindValue(":date", date("Y-m-d "), PDO::PARAM_STR);
        $prepare->bindValue(':unit', $sensorFormat, PDO::PARAM_STR);
        $prepare->bindValue(':value', $sensorValue, PDO::PARAM_INT);
        $prepare->execute();
    }

    /**
     * @param integer $id
     * @return void
     */
    public function delete($id)
    {
        $prepare = $this->connect->prepare("DELETE FROM report WHERE report.id = :id");
        $prepare->bindParam(':id', $id, PDO::PARAM_INT);
        $prepare->execute();
    }
}
