<?php

class ConnectDataBase
{
    public const HOST = 'mysql';
    public const DBNAME = 'database';
    public const USER = 'root';
    public const PASS = 'password';
    /**
     * @var mixed
     */
    public $connect;
    public function __construct()
    {
        $pdo = new PDO("mysql:host=" . self::HOST . ";dbname=" . self::DBNAME, self::USER, self::PASS);
        $this->connect($pdo);
    }

    /**
     * @param mixed $pdo
     *
     * @return mixed
     */
    public function connect($pdo)
    {
        return $this->connect = $pdo;
    }
}
