<?php

require_once "../src/DataBaseReport.php";

class WorkCalculator extends DataBaseSensor
{
    public const FORMAT_FARENHEIT = 1;
    public const FORMAT_CELSIUS = 2;

    /**
     * @return float
     */
    public function calculateCelsius()
    {
        $sensors = $this->getSensor();
        $valueCelsius = [];
        foreach ($sensors as $sensor) {
            if ($sensor->unit == self::FORMAT_CELSIUS) {
                $valueCelsius[] = $sensor->value;
            } elseif ($sensor->unit == self::FORMAT_FARENHEIT) {
                $valueCelsius[] = ($sensor->value - 32) / 1.8;
            }
        }
        return round(array_sum($valueCelsius) / count($valueCelsius), 1);
    }

    /**
     * @return float
     */
    public function calculateFahrenheit()
    {
        $sensors = $this->getSensor();
        $fahrenheit = [];
        foreach ($sensors as $sensor) {
            if ($sensor->unit == self::FORMAT_FARENHEIT) {
                $fahrenheit[] = $sensor->value;
            } elseif ($sensor->unit == self::FORMAT_CELSIUS) {
                $fahrenheit[] = $sensor->value * 1.8 + 32;
            }
        }
        return round(array_sum($fahrenheit) / count($fahrenheit), 1);
    }

    /**
     * @param integer $sensorFormat
     *
     * @return float|void
     */
    public function convertingSensors($sensorFormat)
    {
        if ($sensorFormat == self::FORMAT_CELSIUS) {
            return $this->calculateCelsius();
        }
        if ($sensorFormat == self::FORMAT_FARENHEIT) {
            return $this->calculateFahrenheit();
        }
    }
}
