<?php

require_once "../src/ConnectDataBase.php";

class DataBaseSensor extends ConnectDataBase
{
    /**
     * @return mixed
     */
    public function getSensor()
    {
        $prepare = $this->connect->prepare('SELECT * FROM sensors');
        $prepare->execute();
        return $prepare->fetchALL(PDO::FETCH_OBJ);
    }

    /**
     * @param string $sensorName
     * @param string $sensorFormat
     * @param integer $sensorValue
     * @return void
     */
    public function create($sensorName, $sensorFormat, $sensorValue)
    {
        $prepare = $this->connect->prepare('INSERT INTO sensors( name, unit, value) VALUES ( :name, :unit, :value)');
        $prepare->bindValue(':name', $sensorName, PDO::PARAM_STR);
        $prepare->bindValue(':unit', $sensorFormat, PDO::PARAM_STR);
        $prepare->bindValue(':value', $sensorValue, PDO::PARAM_INT);
        $prepare->execute();
    }

    /**
     * @param integer $id
     * @return void
     */
    public function delete($id)
    {
        $prepare = $this->connect->prepare("DELETE FROM sensors WHERE sensors.id = :id");
        $prepare->bindParam(':id', $id, PDO::PARAM_INT);
        $prepare->execute();
    }

    /**
     * @param string $sensorName
     * @param string $sensorFormat
     * @param string $sensorValue
     * @param integer $sensorId
     * @return void
     */
    public function edit($sensorName, $sensorFormat, $sensorValue, $sensorId)
    {
        $prepare = $this->connect->prepare("UPDATE sensors SET name = :name, unit = :unit, value = :value WHERE sensors.id = :id");
        $prepare->bindValue(':unit', $sensorFormat, PDO::PARAM_INT);
        $prepare->bindValue(':name', $sensorName, PDO::PARAM_STR);
        $prepare->bindValue(':value', $sensorValue, PDO::PARAM_INT);
        $prepare->bindParam(':id', $sensorId, PDO::PARAM_INT);
        $prepare->execute();
    }
}
