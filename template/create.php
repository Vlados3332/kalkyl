<?php require_once "../public/index.php";
$formatCelsius = WorkCalculator::FORMAT_CELSIUS;
$formatFahrenheit = WorkCalculator::FORMAT_FARENHEIT;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php require_once "sidebar.php"; ?>
        <div class="col-sm-12 col-lg-9 border border-dark  p-5 ">
            <form method="post">
                <div class="text-center text-danger"><h5><?= $validationErrorText ?></h5></div>
                <div class="row mb-2">
                    <label for="sensor_name_id" class="col-sm-2 col-form-label">Название</label>
                    <div class="col-sm-10">
                        <input type="text" name="sensor_name" class="form-control" id="sensor_name_id" required>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="input_value_id" class="col-sm-2 col-form-label">Единица</label>
                    <div class="col-sm-10">
                        <input type="number" name="sensor_value" class="form-control" id="input_value_id" required>
                    </div>
                </div>

                <div class="row mb-2">
                    <label for="input_select_id" class="col-sm-2 col-form-label ">Формат датчиков</label>
                    <div class="col-sm-10">
                        <select class="form-select col-sm-10 " name="sensor_unit" aria-label="Default select example"
                                id="input_select_id">
                            <option selected value="<?= $formatCelsius ?>">Цельсий</option>
                            <option value="<?= $formatFahrenheit ?>">Фаренгейт</option>
                        </select>
                    </div>
                </div>
                <div class="mb-3 text-center">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>


    <script src="/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>
