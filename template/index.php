<?php require_once "../src/WorkCalculator.php";
require_once "../src/DataBaseReport.php";
$DataBaseSensor = new DataBaseSensor;
$formatCelsius = WorkCalculator::FORMAT_CELSIUS;
$formatFahrenheit = WorkCalculator::FORMAT_FARENHEIT;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php require_once "sidebar.php"; ?>
        <div class="col-sm-12 col-lg-9 border pb-5 ">

            <div class="card m-auto mt-5 " style="width: 80%;">
                <div class="card-header text-center">
                    <h5>Список датчиков</h5>
                </div>
                <div class="card-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th scope="col">Номер</th>
                            <th scope="col">Название</th>
                            <th scope="col">Единица измерения</th>
                            <th scope="col">Значение</th>
                            <th scope="col">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($DataBaseSensor->getSensor() as $sensor) : ?>
                            <tr>
                                <th scope="row"><?= $sensor->id ?></th>
                                <td><?= $sensor->name ?></td>
                                <td><?php
                                    if ($sensor->unit == $formatCelsius) :
                                        echo "Цельсий";
                                    endif;
                                    if ($sensor->unit == $formatFahrenheit) :
                                        echo "Фаренгейт";
                                    endif;
                                    ?></td>
                                <td><?= $sensor->value ?></td>
                                <td>
                                    <form method="post"><a href="/index.php?pageId=edit&id=<?= $sensor->id ?>"
                                                           class="btn btn-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor"
                                                 class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                            </svg>
                                        </a>
                                        <button name="sensorData" value="<?= $sensor->id ?>" class="btn btn-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor"
                                                 class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>


        <script src="/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>
