<?php require_once "../src/DataBaseSensor.php";
require_once "../src/WorkCalculator.php";
$formatCelsius = WorkCalculator::FORMAT_CELSIUS;
$formatFahrenheit = WorkCalculator::FORMAT_FARENHEIT;
$DataBaseSensor = new DataBaseSensor;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php require_once "sidebar.php"; ?>
        <div class="col-sm-12 col-lg-9 border  pb-5 ">
            <div class="card m-auto mt-5" style="width: 80%;">
                <div class="card-header text-center">
                    <h5>Сформировать отчёт</h5>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="input-group">
                            <label for="input_select_report" class=" col-form-label">Формат датчиков</label>
                            <select class="form-select m-1" name="sensorFormat" aria-label="Default select example"
                                    id="input_select_report">
                                <option selected value="<?= $formatCelsius ?>"> Цельсий</option>
                                <option value="<?= $formatFahrenheit ?>">Фаренгейт</option>
                            </select>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-primary m-auto" type="submit">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card m-auto mt-5 " style="width: 80%;">
                <div class="card-header text-center">
                    <h5>Список датчиков</h5>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Номер</th>
                            <th scope="col">Название</th>
                            <th scope="col">Единица измерения</th>
                            <th scope="col">Значение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($DataBaseSensor->getSensor() as $sensor) :?>
                            <tr>
                                <th scope="row"><?= $sensor->id ?></th>
                                <td><?= $sensor->name ?></td>
                                <td><?php
                                    if ($sensor->unit == $formatCelsius) :
                                        echo "Цельсий";
                                    endif;
                                    if ($sensor->unit == $formatFahrenheit) :
                                        echo "Фаренгейт";
                                    endif; ?></td>
                                <td><?= $sensor->value ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>
